package jgig.simplesite.web.services.impl;

import jgig.simplesite.models.ObjectResponseModel;
import jgig.simplesite.models.PostModel;
import jgig.simplesite.web.services.core.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Created by luyenchu on 11/8/16.
 */
@Component
public class PostServiceImpl implements PostService {
    private static Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);

    @Autowired
    private RestTemplate restTemplate;
    @Value("${jgig.ws.getpost}")
    private String getPostUrl;

    @Override
    public ObjectResponseModel<PostModel> getPost(long id) {
        logger.info("Getting Post {} at {}", id, getPostUrl);
        ObjectResponseModel<PostModel> retValue = restTemplate.getForObject(getPostUrl + id, ObjectResponseModel.class);
        return retValue;
    }
}
