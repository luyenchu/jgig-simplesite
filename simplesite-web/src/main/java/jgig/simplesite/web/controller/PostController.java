package jgig.simplesite.web.controller;

import jgig.simplesite.models.ObjectResponseModel;
import jgig.simplesite.models.PostModel;
import jgig.simplesite.web.services.core.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by luyenchu on 11/8/16.
 */
@Controller
@RequestMapping("/post")
public class PostController {
    @Autowired
    PostService postService;

    @RequestMapping(value = "/byId/{id}", method = GET)
    @ResponseBody
    public ObjectResponseModel<PostModel> hello(@PathVariable("id") long id) {
        ObjectResponseModel<PostModel> post = postService.getPost(id);
        return post;
    }
}
