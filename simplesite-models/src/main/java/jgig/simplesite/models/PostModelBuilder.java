package jgig.simplesite.models;

import java.util.Date;

/**
 * Created by luyenchu on 11/8/16.
 */
public class PostModelBuilder {
    public String content;
    public long id;
    public String title;
    public Date createdTimestamp;
    public Date publishedDate;
    public String state;

    private PostModelBuilder() {
    }

    public static PostModelBuilder aPostModel() {
        return new PostModelBuilder();
    }

    public PostModelBuilder withContent(String content) {
        this.content = content;
        return this;
    }

    public PostModelBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public PostModelBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public PostModelBuilder withCreatedTimestamp(Date createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
        return this;
    }

    public PostModelBuilder withPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
        return this;
    }

    public PostModelBuilder withState(String state) {
        this.state = state;
        return this;
    }

    public PostModelBuilder but() {
        return aPostModel().withContent(content).withId(id).withTitle(title).withCreatedTimestamp(createdTimestamp).withPublishedDate(publishedDate).withState(state);
    }

    public PostModel build() {
        PostModel postModel = new PostModel();
        postModel.setContent(content);
        postModel.setId(id);
        postModel.setTitle(title);
        postModel.setCreatedTimestamp(createdTimestamp);
        postModel.setPublishedDate(publishedDate);
        postModel.setState(state);
        return postModel;
    }
}
