package jgig.simplesite.constants;

/**
 * Created by luyenchu on 11/8/16.
 */
public interface RestError {
    int ERROR_DB = -100;
    int NOT_FOUND = -101;
    int SUCC = 0;
}
