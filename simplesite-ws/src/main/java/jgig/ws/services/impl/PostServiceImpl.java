package jgig.ws.services.impl;

import jgig.simplesite.constants.RestError;
import jgig.simplesite.entities.Post;
import jgig.simplesite.models.ObjectResponseModel;
import jgig.simplesite.models.PostModel;
import jgig.simplesite.repos.PostRepo;
import jgig.ws.services.core.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static jgig.simplesite.models.PostModelBuilder.aPostModel;

/**
 * Created by luyenchu on 11/8/16.
 */
@Service
public class PostServiceImpl implements PostService {
    private static final Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);
    @Autowired
    private PostRepo postRepo;

    @Override
    public ObjectResponseModel<PostModel> getPost(long id) {
        ObjectResponseModel<PostModel> postModel = new ObjectResponseModel<>();
        try {
            Post one = postRepo.findOne(id);
            Objects.requireNonNull(one);
            postModel.setContent(toPostModel(one));
        } catch (NullPointerException e) {
            postModel.setErrorCode(RestError.ERROR_DB);
        } catch (Exception e) {
            logger.error("getPost", e);
            postModel.setErrorCode(RestError.ERROR_DB);
        }
        return postModel;
    }

    private PostModel toPostModel(Post post) {
        return aPostModel()
                .withId(post.id)
                .withContent(post.content)
                .withCreatedTimestamp(post.createdTimestamp)
                .withPublishedDate(post.publishedDate)
                .withTitle(post.title)
                .withState(post.state.name()).build();
    }
}
