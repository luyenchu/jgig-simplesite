package jgig.ws.services.core;

import jgig.simplesite.models.ObjectResponseModel;
import jgig.simplesite.models.PostModel;

/**
 * Created by luyenchu on 11/8/16.
 */
public interface PostService {
    ObjectResponseModel<PostModel> getPost(long id);
}
