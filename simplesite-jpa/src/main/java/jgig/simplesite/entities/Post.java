package jgig.simplesite.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by luyenchu on 11/8/16.
 */
@Entity
@Table(name = "Posts")
public class Post {
    @Id
    @GeneratedValue
    public long id;
    
    public String title;
    @Lob
    public String content;

    @Temporal(TemporalType.TIMESTAMP)
    public Date createdTimestamp;
    @Temporal(TemporalType.TIMESTAMP)
    public Date publishedDate;

    @Enumerated(EnumType.STRING)
    public PostState state;
}
