package jgig.simplesite.entities;

/**
 * Created by luyenchu on 11/8/16.
 */
public enum PostState {
    PUBLISHED, DRAFT, DELETED;
}
