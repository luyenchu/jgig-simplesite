package jgig.simplesite.repos;

import jgig.simplesite.entities.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by luyenchu on 11/8/16.
 */
@Repository
public interface PostRepo extends CrudRepository<Post, Long> {
    
}
