create database simplesite;
create user 'simplesite'@'localhost' identified by 'simplesite';
create user 'simplesite'@'%' identified by 'simplesite';

grant all on simplesite.* to 'simplesite' identified by 'simplesite';
grant all on simplesite.* to 'simplesite'@'%' identified by 'simplesite';